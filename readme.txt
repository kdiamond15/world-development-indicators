How to use this thing:
open up graphingShell, this should bring up a command line prompt. Just enter 1 when it asks and then follow the rest of the instructions as it asks, using allcountries and allvariables.
You can pick any number of countries or a group of countries based on all countries, but I would recommend not going 1 group or 5-6 individual countries. REMEMBER TO TYPE EVERYTHING EXACTLY AS IT IS WRITTEN IN THE FILE, if you do not do this, your variables will bring up ugly red errors and your countries will not appear. 
Pick only one variable from all variables.

PS, this is a work in progress, more graphing options should be available at a later date, along with the opportunity to show more than one variable at a time. 