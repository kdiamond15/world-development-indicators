import matplotlib.pyplot as plt
import csv
import numpy as np
class firsttry:
    def __init__(self,nameList,variable):
        self.graphTheseNames=nameList
        self.variable=variable
        self.val1960=[]
        self.val1961=[]
        self.val1962=[]
        self.val1963=[]
        self.val1964=[]
        self.val1965=[]
        self.val1966=[]
        self.val1967=[]
        self.val1968=[]
        self.val1969=[]
        self.val1970=[]
        self.val1971=[]
        self.val1972=[]
        self.val1973=[]
        self.val1974=[]
        self.val1975=[]
        self.val1976=[]
        self.val1977=[]
        self.val1978=[]
        self.val1979=[]
        self.val1980=[]
        self.val1981=[]
        self.val1982=[]
        self.val1983=[]
        self.val1984=[]
        self.val1985=[]
        self.val1986=[]
        self.val1987=[]
        self.val1988=[]
        self.val1989=[]
        self.val1990=[]
        self.val1991=[]
        self.val1992=[]
        self.val1993=[]
        self.val1994=[]
        self.val1995=[]
        self.val1996=[]
        self.val1997=[]
        self.val1998=[]
        self.val1999=[]
        self.val2000=[]
        self.val2001=[]
        self.val2002=[]
        self.val2003=[]
        self.val2004=[]
        self.val2005=[]
        self.val2006=[]
        self.val2007=[]
        self.val2008=[]
        self.val2009=[]
        self.val2010=[]
        self.val2011=[]
        self.val2012=[]
        self.val2013=[]
        self.val2014=[]
        self.val2015=[]
        self.countryNames=[]
        self.countryIDS=[]
        self.variableNames=[]
        self.variableIDS=[]
        self.listOfYears=[self.val1960,self.val1961,self.val1962,self.val1963,self.val1964,self.val1965,self.val1966,self.val1967,self.val1968,self.val1969,
                          self.val1970,self.val1971,self.val1972,self.val1973,self.val1974,self.val1975,self.val1976,self.val1977,self.val1978,self.val1979,
                          self.val1980,self.val1981,self.val1982,self.val1983,self.val1984,self.val1985,self.val1986,self.val1987,self.val1988,self.val1989,
                          self.val1990,self.val1991,self.val1992,self.val1993,self.val1994,self.val1995,self.val1996,self.val1997,self.val1998,self.val1999,
                          self.val2000,self.val2001,self.val2002,self.val2003,self.val2004,self.val2005,self.val2006,self.val2007,self.val2008,self.val2009,self.val2010,
                          self.val2011,self.val2012,self.val2013,self.val2014,self.val2015]

    def makePlot(self):
        with open('WDI_Data.csv','r') as csvfile:
            plots=csv.reader(csvfile,delimiter=',')
            for row in plots:
                self.countryNames.append((row[0]))
                self.countryIDS.append((row[1]))
                self.variableNames.append(row[2])
                self.variableIDS.append(row[3])
                yearCounter=4
                for year in self.listOfYears:
                    if yearCounter<len(row):
                        year.append(row[yearCounter])
                        yearCounter+=1

        counter=0
        for varname in self.variableNames:
            if varname==self.variable :
                if self.countryNames[counter] in self.graphTheseNames:
                    x=[self.val1960[counter],self.val1961[counter],self.val1962[counter],self.val1963[counter],self.val1964[counter],self.val1965[counter],self.val1966[counter],self.val1967[counter],self.val1968[counter],self.val1969[counter],self.val1970[counter],self.val1971[counter],self.val1972[counter],self.val1973[counter],self.val1974[counter],self.val1975[counter],self.val1976[counter],self.val1977[counter],self.val1978[counter],self.val1979[counter],self.val1980[counter],self.val1981[counter],self.val1982[counter],self.val1983[counter],self.val1984[counter],self.val1985[counter],self.val1986[counter],self.val1987[counter],self.val1988[counter],self.val1989[counter],self.val1990[counter],self.val1991[counter],self.val1992[counter],self.val1993[counter],self.val1994[counter],self.val1995[counter],self.val1996[counter],self.val1997[counter],self.val1998[counter],self.val1999[counter],self.val2000[counter],self.val2001[counter],self.val2002[counter],self.val2003[counter],self.val2004[counter],self.val2005[counter],self.val2006[counter],self.val2007[counter],self.val2008[counter],self.val2009[counter],self.val2010[counter],self.val2011[counter],self.val2012[counter],self.val2013[counter],self.val2014[counter],self.val2015[counter]]
                    for year in x:
                        if(year!=" " and year!=""):
                            year=0
                    newX=[]        
                    for val in x:
                        if(val!="" and val!=" "):
                            newX.append(float(val))
                        else:newX.append(0)
                    x=newX
                    y=[]
                    anotherCounterIGuess=0
                    for val in x:
                        anotherCounterIGuess+=1
                        y.append(anotherCounterIGuess)
                    (plt.plot(y,x,label=self.countryNames[counter]))
            counter+=1
    
        plt.legend()
        plt.title(self.variable)
        plt.show()
