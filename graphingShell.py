import firsttry

whatProgram=input("Would you like to compare countries by a specific variable? If so, enter 1.")
countriesComplete=False
countryList=[]
EUList=["Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary","Ireland",
        "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovak Republic", "Slovenia", "Spain", "Sweden", "United Kingdom"]
Belligerents=["Libya","Afghanistan","Iraq","Pakistan","Syrian Arab Republic","Nigeria","Cameroon","Egypt","Serbia","Kosovo","Bosnia and Herzegovina","Haiti","Somalia"]
subsahall=["Angola","Burundi","Congo", "Cameroon","Central African Republic","Chad","Equatorial Guinea","Gabon",
           "Kenya","Nigeria","Rwanda","Sao Tome and Principe","Tanzania","Uganda","Sudan","Djibouti","Eritrea","Ethiopia","Somalia","Botswana","Comoros",
           "Lesotho","Madagascar","Malawi","Mauritius","Mozambique","Namibia","Seychelles","South Africa","Swaziland","Zambia","Zimbabwe","Benin","Mali","Burkina Faso",
           "Cabo Verde","Cote d'Ivoire","Gambia","Ghana","Guinea","Guinea-Bissau","Liberia","Mauritania","Niger","Senegal","Sierra Leone","Togo"]
lowersubsah=["Angola","Madagascar","Benin","Malawi","Botswana","Mali","Burkina Faso","Mauritania","Burundi","Mauritius","Cabo Verde","Mozambique","Cameroon",
             "Namibia","Central African Republic","Niger","Chad","Nigeria","Comoros","Rwanda","Sao Tome and Principe","Congo","Senegal","Cote d'Ivoire",
             "Sierra Leone","Eritrea","Somalia","Ethiopia","South Africa","Gabon","Gambia","Sudan","Ghana","Swaziland","Guinea","Tanzania","Guinea-Bissau","Togo",
             "Kenya","Uganda","Lesotho","Zambia","Liberia","Zimbabwe"]
arabWorld=["Algeria","Morocco","Bahrain",'Oman','Comoros','Qatar','Djibouti','Saudi Arabia','Egypt','Somalia','Iraq','Sudan','Jordan','Syrian Arab Republic','Kuwait',
           'Tunisia','Lebanon','United Arab Emirates','Libya','West Bank and Gaza','Mauritania','Yemen']
carsmall=['Antigua and Barbuda','Jamaica',"Bahamas",'St. Kitts and Nevis','Barbados','St. Lucia','Belize','St. Vincent and the Grenadines','Dominica','Suriname',
          'Grenada','Trinidad and Tobago','Guyana']
eubalt=['Bulgaria','Lithuania','Croatia','Poland','Czech Republic','Romania','Estonia','Slovak Republic','Hungary','Slovenia','Latvia']
eapaclow=['American Samoa','Myanmar','Cambodia','Palau','China','Papua New Guinea','Fiji','Philippines','Indonesia','Samoa','Kiribati','Solomon Islands',
          'Thailand','Lao PDR','Timor-Leste','Malaysia','Tonga','Marshall Islands','Tuvalu','Micronesia','Vanuatu','Mongolia','Vietnam']
eapacall=['American Samoa','Myanmar','Cambodia','Palau','China','Papua New Guinea','Fiji','Philippines','Indonesia','Samoa','Kiribati','Solomon Islands',
          'Thailand','Lao PDR','Timor-Leste','Malaysia','Tonga','Marshall Islands','Tuvalu','Micronesia','Vanuatu','Mongolia','Vietnam','Korea','Japan','Singapore',
          'Taiwan','New Zealand','Australia','Hong Hong SAR']
EUzone=["Austria", "Belgium","Cyprus","Estonia", "Finland", "France", "Germany", "Greece","Ireland","Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands","Portugal", "Slovak Republic", "Slovenia", "Spain"]
eurocendev=['Albania','Macedonia','Armenia','Moldova','Azerbaijan','Montenegro','Belarus','Romania','Bosnia and Herzegovina','Serbia','Bulgaria','Tajikistan','Georgia',
            'Turkey','Kazakhstan','Turkmenistan','Kosovo','Ukraine','Kyrgyz Republic','Uzbekistan']
eurocenall=['Albania','Macedonia','Armenia','Moldova','Azerbaijan','Montenegro','Belarus','Romania','Bosnia and Herzegovina','Serbia','Bulgaria','Tajikistan','Georgia',
            'Turkey','Kazakhstan','Turkmenistan','Kosovo','Ukraine','Kyrgyz Republic','Uzbekistan','Greece','Russian Federation','Cyprus']
HIPClist = ['Afghanistan','Benin','Bolivia','Burkina Faso','Burundi','Cameroon','Central African Republic','Chad','Congo','Comoros',"Cote d'Ivoire",'Ethiopia',
            'Gambia','Ghana','Guinea','Guinea-Bissau','Guyana','Haiti','Honduras','Liberia','Madagascar','Mali','Mauritania','Mozambique','Nicaragua','Niger','Rwanda',
            'Sao Tome and Principe','Senegal','Sierra Leone','Tanzania','Togo','Uganda','Zambia']
latcarall=['Antigua and Barbuda','Aruba','Bahamas','Barbados','Cayman Islands','Cuba','Dominica','Dominican Republic','Grenada','Haiti','Jamaica',
           'Puerto Rico','St. Kitts and Nevis','St. Lucia','St. Vincent and the Grenadines','Trinidad and Tobago','Turks and Caicos Islands',
           'Virgin Islands','Belize','Costa Rica','El Salvador','Guatemala','Honduras','Mexico','Nicaragua','Panama','Argentina','Bolivia','Brazil','Chile','Colombia',
           'Ecuador','Guyana','Paraguay','Peru','Suriname','Uruguay','Venezuela']
latcardev=['Belize','Guyana','Bolivia','Haiti','Brazil','Honduras','Colombia','Jamaica','Costa Rica','Mexico','Cuba','Nicaragua','Dominica','Panama','Dominican Republic',
           'Paraguay','Ecuador','Peru','El Salvador','St. Lucia','Grenada','St. Vincent and the Grenadines','Guatemala','Suriname']
leastdeveloped=['Afghanistan','Madagascar','Angola','Malawi','Bangladesh','Mali','Benin','Mauritania','Bhutan','Mozambique','Burkina Faso','Myanmar','Burundi',
                'Nepal','Cambodia','Niger','Central African Republic','Rwanda','Chad','Sao Tome and Principe','Comoros,Senegal','Congo','Sierra Leone',
                'Djibouti','Solomon Islands','Equatorial Guinea','Somalia','Eritrea','Ethiopia','Sudan','Gambia','Tanzania','Guinea','Timor-Leste',
                'Guinea-Bissau','Togo','Haiti','Tuvalu','Kiribati','Uganda','Lao PDR','Vanuatu','Lesotho','Yemen','Liberia','Zambia']
MENAdev=['Algeria','Libya','Djibouti','Morocco','Egypt','Syrian Arab Republic','Iran','Tunisia','Iraq','West Bank and Gaza','Jordan','Yemen','Lebanon']
MENAall=['Algeria','Libya','Djibouti','Morocco','Egypt','Syrian Arab Republic','Iran','Tunisia','Iraq','West Bank and Gaza','Jordan','Yemen','Lebanon','Bahrain',
         'Kuwait','United Arab Emirates','Oman','Israel','Qatar','Saudi Arabia','Turkey']
MENAdeveloping=['United Arab Emirates','Oman','Israel','Qatar','Saudi Arabia','Turkey']
OECDmems=['Australia','Austria','Belgium','Canada','Chile','Czech Republic','Denmark','Estonia','Finland','France','Germany','Greece','Hungary','Ireland','Iceland',
          'Israel','Italy','Japan','Korea','Luxembourg','Mexico','Netherlands','New Zealand','Norway','Poland','Portugal','Slovakia','Slovenia','Spain','Sweden',
          'Switzerland','Turkey','United Kingdom','United States']
OPECmems=['Algeria','Angola','Ecuador','Indonesia','Iran','Iraq','Kuwait','Libya','Nigeria','Qatar','Saudi Arabia','United Arab Emirates','Venezuela']
smallstates=['Antigua and Barbuda','Marshall Islands','Bahamas', 'Mauritius','Barbados','Micronesia','Belize','Montenegro','Bhutan','Namibia','Botswana',
             'Palau','Cabo Verde','Samoa','Comoros','Sao Tome and Principe','Djibouti','Seychelles','Dominica','Solomon Islands','Equatorial Guinea',
             'St. Kitts and Nevis','Fiji','St. Lucia','Gabon','St. Vincent and the Grenadines','Gambia', 'Suriname','Grenada','Swaziland','Guinea-Bissau','Timor-Leste',
             'Guyana','Tonga','Jamaica','Trinidad and Tobago','Kiribati','Tuvalu','Lesotho','Vanuatu','Maldives']
predefinedLists={'EUList':EUList,'Belligerents':Belligerents,'subsahall':subsahall,'lowersubsah':lowersubsah,'arabWorld':arabWorld,'carsmall':carsmall,
                 'eubalt':eubalt,'eapacall':eapacall,'EUzone':EUzone,'eurocendev':eurocendev,'eurocenall':eurocenall,'HIPClist':HIPClist,
                 'latcarall':latcarall,'latcardev':latcardev,'leastdeveloped':leastdeveloped,'MENAdev':MENAdev,'MENAall':MENAall,'MENAdeveloping':MENAdeveloping,
                 'OECDmems':OECDmems,'OPECmems':OPECmems,'smallstates':smallstates}

if(whatProgram=="1"):
    while(countriesComplete==False):
        nextCountry=input("Enter in a country to look at. If you are finished, enter 'done'. If you want to look at the whole world, type 'World'. ")
        if(nextCountry!="done" and nextCountry!="World"):
            if(nextCountry in predefinedLists):
                print("This list includes: ")
                for country in predefinedLists.get(nextCountry):
                    print(country)
                answer=input("If this is not okay, enter 'no'. Otherwise, type anything to continue")
                if(answer!='no'):
                    for country in predefinedLists.get(nextCountry):
                        countryList.append(country)                    
            else:
                countryList.append(nextCountry)
        elif(nextCountry=="World"):
            countryList.append("World")
            countriesComplete=True
        else:
            if(len(countryList)>0):
                print("Okay. Let's move on.")
                countriesComplete=True
            else:
                print("You haven't entered any countries yet! Please try again.")
    nextVariable=input("What variable do you want to look at? ")
    #run firsttry with these inputs
    ft=firsttry.firsttry(countryList, nextVariable)
    ft.makePlot()
